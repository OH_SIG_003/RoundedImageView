# RoundedImageView

## Introduction
RoundedImageView is a versatile ImageView that supports rounded corners, ovals, or circles. It offers numerous additional features such as rounded rectangles, ScaleTypes, and TileModes.

## Effect

<img src="./image/image_bitmap.png"/>

<img src="./image/image_ovals.png"/>

<img src="./image/image_color.png"/>

<img src="./image/image_background.png"/>

<img src="./image/image_svg.png"/>

## How to Install

 ```
 ohpm install @ohos/roundedimageview
 ```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitcode.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

1. Import RoundedImageView on the target page.

 ```
   import { RoundedImageView, ScaleType, TileMode, SrcType, FileUtils } from '@ohos/roundedimageview/'
 ```

2. Create multiple **PictureItem** instances for constructing data, with similar usage methods.

3. Initialization: Instantiate **dialogController** and the corresponding **RoundedImageView.Model** objects, and add **typeArr** types to mark the current page type.

 ```
  dialogController: CustomDialogController = new CustomDialogController({
    alignment: DialogAlignment.Top,
    builder: TypeCustomDialog({ typeValue: $typeValue }),
    autoCancel: true
  })
  private initViewModels(): void  {
    let viewModelsLength = Math.max(this.picIdxArr.length, this.colorIdxArr.length)
    for (var index = 0; index < viewModelsLength; index++) {
      this.viewModels.push(new RoundedImageView.Model)
    }
  }
 ```

4. Set UI properties through a **Model** object to define the required style.

 ```
  private updateViewModels(pictureItem: PictureItem[]) {
    pictureItem.forEach((val, idx) => {
      this.viewModels[idx]
        .setImageSrc(pictureItem[idx].src)
        .setBackgroundColor(pictureItem[idx].backgroundColor)
        .setSrcType(pictureItem[idx].srcType)
        .setIsSvg(pictureItem[idx].isSvg)
        .setTypeValue(this.typeValue)
        .setUiWidth(pictureItem[idx].uiWidth)
        .setUiHeight(pictureItem[idx].uiHeight)
        .setScaleType(pictureItem[idx].scaleType)
        .setTileModeXY(pictureItem[idx].tileMode)
        .setCornerRadius(pictureItem[idx].cornerRadius)
        .setBorderWidth(pictureItem[idx].borderWidth)
        .setBorderColor(pictureItem[idx].borderColor)
        .setPadding(pictureItem[idx].padding)
        .setColorWidth(this.uiHeight)
        .setColorHeight(this.uiHeight)
    });
  }
 ```

5. Draw the UI as follows: Place the type selection content at the top, and listen for changes in **type_value** to rebuild the **Model** object and notify the **Model** class. Use a list layout within **Scroll** to place images.

 ```
 build() {
    Column() {
      Column() {
        Image($r('app.media.down')).width(30).height(30).position({ x: -30, y: 5 }).onClick(() => {
          this.dialogController.open()
        })
        Text(' select:' + this.typeValue).fontSize(30)
      }.margin(15)

      Scroll(this.scroller) {
        List({ space: 10, initialIndex: 0 }) {
          if (this.typeValue == 'Bitmap') {
            ForEach(this.picIdxArr, (item) => {
              ListItem() {
                this.viewItem(this.viewModels[item], this.rectPictureItems[item])
              }.editable(false)
            }, item => item)
          }
          ...
        }
      }
      .scrollable(ScrollDirection.Vertical).scrollBar(BarState.Off)
    }
    .width('100%')
    .height('100%')
    .backgroundColor(0xDCDCDC)
    .padding({ top: 20, bottom: 100 })
  }
 ```

For details, see the implementation on the sample page of the RoundedImageView library.

## Available APIs

`let data: RoundedImageView.Model  = new RoundedImageView.Model();`
1. Sets the image path.
   `data.setImageSrc(src: string | Resource | ArrayBuffer)`

2. Sets the image type.
   `data.setSrcType(srcType: SrcType)`

3. Sets the scale type of the image.
   `data.setScaleType(scaleType: ScaleType)`

4. Sets how the image is repeated.
   `data.setTileModeXY(value: TileMode)`

5. Sets the corner radius.
   `data.setCornerRadius(cornerRadius: number)`

6. Sets the width of the image.
   `data.setWidth(width: number)`

7. Sets the height of the image.
   `data.setHeight(height: number)`

8. Sets the border width.
   `data.setBorderWidth(borderWidth: number)`

9. Sets the background color.
   `data.setBackgroundColor(value: string | CanvasGradient | CanvasPattern)`

10. Sets the context.

   `data.setContext(context:common.UIAbilityContext)`


## About obfuscation
- Code obfuscation, please see[Code Obfuscation](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- If you want the roundedimageview library not to be obfuscated during code obfuscation, you need to add corresponding exclusion rules in the obfuscation rule configuration file obfuscation-rules.txt：
```
-keep
./oh_modules/@ohos/roundedimageview
```

## Constraints
This project has been verified in the following versions:

- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API 12 Release (5.0.0.66)

HSP adaptation:

When calling the **setContext(context:common.UIAbilityContext)** API in **RoundedImageView.Model** for an HSP, you must pass the correct context to ensure that third-party libraries can subsequently access the resources correctly.

This context can be omitted for non-HSP scenarios.

## Directory Structure

 ```
|---- RoundedImageView
|     |---- entry                                                         # Sample code
|     |---- library                                                       # RoundedImageView library
|           |---- src     
|                   |---- main 
|                           |---- ets 
|                                   |---- components
|                                             |---- DownloadUtils.ts      # Image download utility
|                                             |---- FileUtils.ts          # File operation utility
|                                             |---- PixelMapUtils.ts      # PixelMap utility
|                                             |---- RoundedImageView.ets  # Core implementation of the library
|                                             |---- ScaleType.ts          # ScaleType enumeration
|                                             |---- SrcType.ts            # SrcType enumeration
|                                             |---- TileMode.ts           # TileMode enumeration
|                                             |---- GlobalContext.ts      # GlobalContext as a substitute for globalThis
|     |---- README.md                                                     # Readme 
 ```

## How to Contribute

If you find any problem when using the project, submit an [issue](https://gitcode.com/openharmony-sig/RoundedImageView/issues) or a [PR](https://gitcode.com/openharmony-sig/RoundedImageView/pulls).

## License

This project is licensed under [Apache License 2.0](./LICENSE).
